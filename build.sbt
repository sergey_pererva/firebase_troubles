name := "firebase-test"

version := "0.1"

resolvers += Resolver.sonatypeRepo("releases")

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  "com.firebase" % "firebase-client-jvm" % "2.5.0"
)

