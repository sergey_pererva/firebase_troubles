import com.firebase.client.Firebase
import com.firebase.client.Firebase.AuthListener
import com.firebase.client.FirebaseError
import com.firebase.client._
import com.firebase.client.Firebase.AuthListener

import _root_.scala.concurrent.Future
import _root_.scala.concurrent._

object Test{
  var i = 0

  def main(args: Array[String]) = {
    println("Hi!")

    nestConnection("doesn't matter")
  }

  def nestConnection(token: String): Unit = {
    val conn = new Firebase("https://developer-api.nest.com")
    i += 1

    if (i < 10) {
      conn.auth(token, new AuthListener {
        override def onAuthError(err: FirebaseError): Unit = {
          println("!!!AUTH ERROR"); nestConnection(token)
        }

        override def onAuthRevoked(err: FirebaseError): Unit = {
          println("!!!AUTH REVOKED"); nestConnection(token)
        }

        override def onAuthSuccess(o: Any): Unit = {
          println("!!!SUCCESS")
        }
      })
    }

  }
}
